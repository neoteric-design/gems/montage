# 1.5.0

* Replace paperclip dependency with kt-paperclip if any projects need to continue using this

# 1.4.0

* Replace outdated `updated_attributes` with plain `update`. Should be backwards compatible but bump minor version just in case
