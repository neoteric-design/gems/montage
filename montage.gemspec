$:.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'montage/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'montage'
  s.version     = Montage::VERSION
  s.authors     = ['Nic Aitch', 'Joe Sak', 'Madeline Cowie']
  s.email       = ['nic@nicinabox.com', 'joe@joesak.com', 'madeline@cowie.me']
  s.homepage    = 'http://neotericdesign.com'
  s.summary     = 'Drag n drop image gallery uploader'
  s.description = 'Drag n drop image gallery uploader'

  s.files = Dir['{app,config,db,lib,vendor}/**/*'] + ['Rakefile', 'README.md']
  s.test_files = Dir['test/**/*']

  s.add_dependency 'kt-paperclip', '>= 6.1.0'
  s.add_dependency 'rails', '> 4'

  s.add_development_dependency 'sqlite3'
end
