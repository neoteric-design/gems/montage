class CreateExampleRecords < ActiveRecord::Migration
  def change
    create_table :example_records do |t|
      t.text :image_urls

      t.timestamps
    end
  end
end
