class ExampleRecordsController < ApplicationController
  def index
    @example_records = ExampleRecord.all
  end

  def show
  end

  def new
    @example_record = ExampleRecord.new
  end

  def create
    @example_record = ExampleRecord.new(params[:example_record])
    if @example_record.save
      redirect_to example_records_path, :notice => 'saved!'
    else
      render :new
    end
  end

  def edit
  end

  def update
  end

  def destroy
  end
end
