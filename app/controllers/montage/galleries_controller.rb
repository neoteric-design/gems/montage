module Montage
  class GalleriesController < MontageController
    def create
      gallery = ::Montage::Gallery.new(gallery_params)

      if gallery.save
        render :json => gallery
      else
        render :json => gallery.errors
      end
    end

    def update
      render :nothing => true
    end

    private

    def gallery_params
      params.require(:gallery).permit(:title, :image_ids)
    end
  end
end
