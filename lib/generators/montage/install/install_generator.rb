module Montage
  class InstallGenerator < ::Rails::Generators::Base
    source_root File.expand_path("../templates", __FILE__)

    def mount_engine
      route "mount ::Montage::Engine => '/montage'"
    end

    def copy_files
      copy_file "configuration.rb", "config/initializers/montage.rb"
      copy_file "paperclip.rb",     "config/initializers/paperclip.rb"
    end

    def install_migrations
      rake "montage:install:migrations"

      # unless images_already_belong_to_galleries?
      #   generate image_gallery_id_migration
      # end
    end

    private
    def images_already_belong_to_galleries?
      ::ActiveRecord::Migration.table_exists?(:montage_images) &&
      ::ActiveRecord::Migration.column_exists?(:montage_images,
                                               :gallery_id)
    end

    def image_gallery_id_migration
      "migration AddGalleriesToMontageImages gallery_id:integer:index"
    end
  end
end
