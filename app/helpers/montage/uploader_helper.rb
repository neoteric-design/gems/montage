module Montage
  module UploaderHelper
    def uploader_path(form_builder)
      if form_builder.respond_to?(:inputs)
        "montage/formtastic_uploader"
      else
        "montage/default_uploader"
      end
    end
  end
end
