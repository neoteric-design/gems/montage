module Montage
  class Configuration
    attr_accessor :authentication_method

    def initialize
      self.authentication_method ||= :authenticate_admin_user!
    end
  end
end
