Montage::Engine.routes.draw do
  resources :images, :only => [:create, :update, :edit, :show] do
    post 'sort' => 'images#sort', :on => :collection, :as => :sort
  end

  resources :galleries, :only => [:create, :update]
end
