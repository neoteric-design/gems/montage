module Montage
  class Gallery < ::ActiveRecord::Base
    self.table_name = :montage_galleries

    belongs_to :gallery_attachable,
               :polymorphic => true,
               :required => false

    has_many :images, :class_name => 'Montage::Image'

    def key_image
      images.first
    end

    def key_image_url(style = :original)
      key_image.url(style) if key_image
    end
  end
end
