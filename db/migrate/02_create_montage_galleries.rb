class CreateMontageGalleries < ActiveRecord::Migration
  def change
    create_table :montage_galleries do |t|
      t.references :gallery_attachable,
                   :polymorphic => true
      t.string :title
    end
  end
end
