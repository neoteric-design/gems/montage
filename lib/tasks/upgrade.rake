namespace :montage do
  desc "Upgrade montage from .images to .galleries"
  task :upgrade_images_to_galleries => :environment do
    ActiveRecord::Base.descendants.each do |model|
      if model.new.respond_to?(:galleries)
        model.find_each do |record|
          image_ids = Montage::Image.where({
            :gallery_attachable_type => model.name,
            :gallery_attachable_id   => record.id
          }).collect(&:id)

          next unless image_ids.any?

          begin
            record.galleries.create!(:image_ids => image_ids)
            print "Created gallery for #{model}##{record.id}!\n"
          rescue => e
            print "Could not create gallery for #{model}##{record.id}\n"
            print "#{e}\n"
          end
        end
      end
    end
  end
end
