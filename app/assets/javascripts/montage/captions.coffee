$ ->
  $(document).on 'click', '.preview .thumb', (e) ->
    e.preventDefault()
    $caption       = $(this).parents('.container').find('.caption-text')
    $image         = $(this).find('img')
    $caret         = $caption.find('.fa-caret-up')
    caption_text   = $image.data('caption')

    $caption.fadeIn()

    $caption.css
      top: $(this).height()

    $caret.css
      left: $(this).offset().left - $caption.offset().left +
            ($(this).outerWidth(true) / 2) - ($caret.width() / 2)

    $caption.find('input').val(caption_text).focus()
    $caption.data('image', $image)

  $(document).on 'click', '.caption-text .save-caption', (e) ->
    console.log("clicked save caption")
    e.preventDefault()
    $(this).trigger('save.caption')

  $(document).on 'save.caption', '.caption-text', (e) ->
    $caption     = $(this)
    $image       = $caption.data('image')
    caption_text = $caption.find('input').val()

    $.post $image.data('update-url'),
      _method: 'PUT'
      image:
        caption: caption_text
    , (data) ->
      $image.data('caption', data.caption)
      $caption.fadeOut ->
        $caption.find('input').val('')

    false

  $(document).on 'keypress', '.caption-text input', (e) ->
    if e.keyCode == 13
      $(e.target).trigger('save.caption')
      false

  $(document).on 'click', '.caption-text .cancel-caption', (e) ->
    e.preventDefault()
    $caption = $(this).parent('.caption-text')

    $caption.fadeOut().find('input').val('')
    $.removeData($caption[0], 'image')