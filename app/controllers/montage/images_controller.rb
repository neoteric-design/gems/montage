module Montage
  class ImagesController < MontageController
    def create
      image = ::Montage::Image.create!(:photo => create_params[:file],
                                       :gallery_id => create_params[:gallery_id])
      render :json => {
        :filelink => image.url(:thumb),
        :id => image.id
      }
    end

    def show
      @image = ::Montage::Image.find(params[:id])
      render layout: false
    end

    def edit
      @image = ::Montage::Image.find(params[:id])
      render layout: false
    end

    def update
      image = ::Montage::Image.find(params[:id])

      if image.update(image_params)
        respond_to do |format|
          format.html { redirect_to image }
          format.js   { render json: image }
        end
      else
        render :json => { :errors => image.errors }
      end
    end

    def sort
      images = params[:images] || []

      if images.any?
        images.each_with_index do |id, index|
          Montage::Image.where(id: id).update_all(position: index + 1)
        end
      end

      head :ok
    end

    private

    def create_params
      params.permit(:gallery_id, :file, :utf8, :authenticity_token)
    end

    def image_params
      params.require(:image).permit(:caption, :gallery_id, :crop_x, :crop_y, :crop_w, :crop_h)
    end
  end
end
