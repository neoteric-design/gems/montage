Dummy::Application.routes.draw do
  mount Montage::Engine => '/montage'

  # for testing only
  resources :example_records
  root :to => 'example_records#new'
end
