module Montage
  class Engine < ::Rails::Engine
    isolate_namespace Montage

    initializer 'montage.action_controller',
                :before => :load_config_initializers do
      ::ActiveSupport.on_load :action_controller do
        ::ActionController::Base.send(:helper, UploaderHelper)
      end
    end

    initializer 'montage.extend_active_record',
                :after => :load_config_initializers do
      ::ActiveSupport.on_load :active_record do
        ::ActiveRecord::Base.extend(::Montage::ActiveRecord::ClassMethods)
      end
    end
  end
end
