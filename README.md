# ⚠️ Archival Notice ⚠️ 

This project was a part of Neoteric Design's internal tooling and prototypes. 

These are no longer officially supported or maintained, and may contain bugs or be in any stage of (in)completeness.

This repository is provided as a courtesy to our former clients to support their projects going forward, as well in the interest of giving back what we have to the community. If you found yourself here, we hope you find it useful in some capacity ❤️ 


--------------------

# Montage

The Neoteric Design, Inc. gem for image gallery management

## Install

    rails g montage:install

### Model

    class MyModel < ActiveRecord::Base
      montage
    end

### SCSS

    @import "montage";

### JS

    //= require montage

### Form

Update title, specs, aspect, size, and id as necessary.
Size (minimum) and aspect are optional and passed to Jcrop and enforced there: http://deepliquid.com/content/Jcrop_Manual.html#Setting_Options

    <%= render 'montage/form',
               :f      => f,
               :title  => "Key Image",
               :id     => "some_id_for_your_needs",
               :size   => "500x200",
               :aspect => "16 / 9",
               :specs  => "image specs here" %>
