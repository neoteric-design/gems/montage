module Montage
  class Image < ::ActiveRecord::Base
    self.table_name = :montage_images

    default_scope -> { order(:position) }

    belongs_to :gallery, required: false

    has_attached_file :photo, :styles => {
                                :cropped => { :processors => [:crop], :geometry => 'cool' },
                                :large => "700x700>",
                                :medium => "300x300>",
                                :thumb => "150x150>",
                                :square_thumb => "150x150#"
                              },
                              :default_url => "/images/:style/missing.png"

    validates_attachment_content_type :photo, :content_type => /\Aimage/

    delegate :url, :to => :photo,
                   :prefix => false,
                   :allow_nil => true


    attr_accessor :crop_x, :crop_y, :crop_w, :crop_h
    after_update :reprocess_photo, :if => :cropping?

    def cropping?
      crop_x.present? && crop_y.present? && crop_w.present? && crop_h.present?
    end

    def image_geometry(attachment_name, style = :original)
      @geometry ||= {}
      path = (self.send(attachment_name).options[:storage] == :s3) ? self.send(attachment_name).url(style) : self.send(attachment_name).path(style)
      @geometry[style] ||= Paperclip::Geometry.from_file(path)
    end

    private

    def reprocess_photo
      photo.assign(photo)
      photo.save
    end
  end
end
