module Montage
  class MontageController < ::ApplicationController
    if Rails::VERSION::MAJOR >= 5
      before_action :montage_authentication
    else
      before_filter :montage_authentication
    end

    private
    def montage_authentication
      self.send(Montage.authentication_method)
    end
  end
end
