require "paperclip"

module Paperclip
  class Crop < Thumbnail
    def transformation_command
      if @attachment.instance.cropping?
        (crop_command.concat super).join(' ').gsub(/ -crop \S+/, '').split(' ')
      else
        super
      end
    end

    def crop_command
      target = @attachment.instance

      if target.cropping?
        w = target.crop_w
        h = target.crop_h
        x = target.crop_x
        y = target.crop_y
        target.crop_x = target.crop_y = target.crop_w = target.crop_h = nil

        ["-crop", "#{w}x#{h}+#{x}+#{y}!"]
      end
    end
  end
end