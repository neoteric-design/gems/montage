class CreateMontageImages < ::ActiveRecord::Migration
  def change
    create_table :montage_images do |t|
      t.references :gallery, :index => true
      t.string     :caption
      t.integer    :position, :index => true
    end

    add_attachment :montage_images, :photo
  end
end
