require 'montage'

Montage.configure do |config|
  config.authentication_method = :authenticate_admin_user!
end
