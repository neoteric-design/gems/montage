require "paperclip"
require "paperclip_processors/crop"

require 'montage/configuration'
require 'montage/active_record'
require 'montage/engine'

module Montage
  def self.configure
    yield(configuration)
  end

  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.authentication_method
    configuration.authentication_method
  end
end
