module Montage
  module ActiveRecord
    module ClassMethods
      def montage(*gallery_names)
        include InstanceMethods

        has_many :galleries,
                 :class_name => '::Montage::Gallery',
                 :as         => :gallery_attachable

        has_many :images, :through => :galleries

        gallery_names.each do |name|
          define_method name do
            galleries.where(title: name.to_s).first_or_initialize
          end
        end
      end

      alias_method :neoteric_uploader_enabled, :montage
    end

    module InstanceMethods
      attr_reader :key_image, :montage_gallery

      def key_image
        images.first
      end

      def key_image_url(style = :original)
        key_image.url(style) if key_image
      end

      def montage_gallery
        @montage_gallery ||= galleries.first_or_initialize(title: 'montage_gallery')
      end
    end
  end
end
